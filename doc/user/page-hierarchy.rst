
.. _misc:

==============================================================================
Users
==============================================================================

- @subpage what_is_libinput
- @subpage faq
- @subpage tools
- @subpage reporting_bugs

.. _touchpads:

==============================================================================
Touchpads
==============================================================================

- @subpage scrolling
- @subpage clickpad_softbuttons
- @subpage tapping
- @subpage gestures
- @subpage touchpad_pressure
- @subpage palm_detection
- @subpage t440_support
- @subpage touchpad_jumping_cursor
- @subpage absolute_coordinate_ranges
- @subpage touchpad_jitter

.. _touchscreens:

==============================================================================
Touchscreens
==============================================================================

- @subpage absolute_axes

.. _pointers:

==============================================================================
Mice, Trackballs, etc.
==============================================================================

- @subpage motion_normalization
- @subpage middle_button_emulation
- @subpage button_debouncing
- @subpage trackpoints

.. _tablets:

==============================================================================
Graphics Tablets
==============================================================================

- @subpage tablet-support

.. _other_devices:

==============================================================================
Other devices
==============================================================================

- @subpage switches

.. _developers:

==============================================================================
Developers
==============================================================================

Contributions to libinput are always welcome. See the links below for
specific documentation.

- @subpage what_is_libinput
- @subpage contributing
- @subpage architecture
- @subpage building_libinput
- @subpage test-suite
- @subpage tools
- @subpage pointer-acceleration
- @subpage device-quirks
- @subpage udev_config
- @subpage seats
- @subpage timestamps
